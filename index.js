const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4005;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://tjtagana:6wopfEZlAmfnw2rw@zuitt-bootcamp1.x4smuop.mongodb.net/s35-activity?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("We're connected to the cloud database"));

const signupSchema = new mongoose.Schema({
	username : String,
	password : String
})

const Signup = mongoose.model("Signup", signupSchema);

app.post("/signup", (request, response) => {
	let newSignup = new Signup
	({ username: request.body.username, password: request.body.password })

		newSignup.save((error, savedSignup) => {
			if(error){
				return console.error(error)
			} else {
				return response.status(200).send('New user registered!')
			}
		})
	})

app.listen(port, () => console.log(`Server is now running at port ${port}`));